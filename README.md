## Deploy Portainer

Before deploying the Traefik, the Portainer stack must be modified.

## Delete field

The field to delete is the one that refers to the ports, since Portainer already has them specified.
